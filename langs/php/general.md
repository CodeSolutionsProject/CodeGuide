- Version 0.1 -

#### General grammar of lang

- All lines finish with a semicolon (;)

- Vars are append with a dollar sign ever -> $x is x variable

- Vars aren't defined before use. Doesn't name Int,String,.... ever

- When echoing or using functions, you can use 'text' or "text", but in first ones you can't embed a var. Example:

    'Hello $x' -> Hello $x

    "Hello $x" -> Hello World

#### Comments

```
// This is a single line comment
/*  This is
    a multiline comment */
```

#### Types of vars

Nothing to show here.
