<?php
$site = 'funcs'; /* For menus */
$func = isset($_GET['func']) ? $_GET['func'] : '';

/* Set lang to see */
$lang = isset($_GET['lang']) ? $_GET['lang'] : 'php';

/* Show header */
require_once 'assets/html/header.php';

/* For searches */
echo '<div id="searchresult">';
echo '</div>';

if ($func != ""){
    require_once "lib/parsedown.php";
    $file = "langs/$lang/functions/$func.json";
    if (file_exists($file)){
        $fulljson = json_decode(file_get_contents($file), true);
        /* Show function */
        if (!isset($fulljson['input'])){ $fulljson['input'] = null; }
        if (!isset($fulljson['output'])){ $fulljson['output'] = null; }
        seefunc($lang,$func,$fulljson['visual'],$fulljson['description'],$fulljson['parameters'],$fulljson['example'],$fulljson['input'],$fulljson['output']); 
    }else{
        echo "<p>No existe la función</p>";
    }

} else { /* Function search */
    echo '<div id="nosearch">';
    echo '<p>Mostrando todas las funciones...</p>';
    $funcs = scandir("langs/$lang/functions/");
    foreach ($funcs as $file){
        $file = explode('.',$file)[0];
        echo "<p><a href='?lang=$lang&func=$file'>$file</a></p>";
    }
    echo '</div>';
}


require_once 'assets/html/footer.php';


function seefunc($lang,$func,$visual,$description,$parameters,$example,$input,$output) {
    echo "<h3>$lang - Function $func</h3>";
    echo "<pre><code class='$lang'>$visual</code></pre>";
    ?>
    <h4>Description: </h4>
    <p><?php print $description;?></p>
    <?php
    /* Run for all params to show it */
    
    foreach($parameters as $param => $descr)
    {
        echo "<p><b><u>$param</u></b>: $descr</p>";
    }
    ?>
    <h4>Example</h4>
    <h5>Code</h5>
    <pre><code class="<?php print $lang?>"><?php 
    foreach($example as $x)
    {
        echo "$x".PHP_EOL;
    }
    ?>
    </code></pre>
    <?php
    if (isset($input)){
        echo '<h5>Input for example: </h5><p>'.$input.'</p>';
    }
    if (isset($output)){
        echo '<h5>Output of example: </h5><p>'.$output.'</p>';
    }    
}
?>