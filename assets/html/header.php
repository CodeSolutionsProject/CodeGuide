<?php
$commonAssets = '../assets/'; /* It loads from root path */
?>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="<?php echo $commonAssets ?>css/bootstrap-paper.css">
    <link rel="stylesheet" type="text/css" href="assets/css/codesee.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $commonAssets ?>css/codeStyles/vs.css">
    <script src="<?php echo $commonAssets ?>js/highlight.pack.js"></script>
    
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">CodeGuide</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
      <ul class="nav navbar-nav">
        <li <?php if ($site == 'index'){ echo 'class="active"';} ?> >
          <a href="index.php">Index</a>
        </li>
        <li <?php if ($site == 'funcs'){ echo 'class="active"';} ?> >
          <a href="funcs.php">Functions Helper</a>
        </li>
        <!--<li <?php if ($site == 'ginfo'){ echo 'class="active"';}?> >
        <a href="index.php?page=ginfo">General info of a lang</a>
        </li>-->
        <li><a href="../">CodeShare, snippets of code</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="proyecto.jkanetwork.com">JKANetwork</a></li>
      </ul>
    </div>
  </div>
</nav>


<!-- Start of content (Container div in Bootstrap), after nav -->
<div class="container">


<div class="panel panel-default">
  <div class="panel-heading">Welcome to CodeGuide, fast help to programmers</div>
  <div class="panel-body">
  <?php 
if ($site == 'index'){
?>
        <h4>Select lang to show.</h4>
        <p>You can see how different works a lang from another, or simple things to program in your lang,
           as a fast guide to see when you don't remember something</p>
        <form method='get'>
            <p>Lang to show:
              <input type="radio" name="lang" value="kotlin" <?php if ($lang == 'kotlin'){echo 'checked="checked"';} ?>>Kotlin
              <input type="radio" name="lang" value="php" <?php if ($lang == 'php'){echo 'checked="checked"';} ?>>PHP
            </p>
            <p>What to see:
              <input type="radio" name="see" value="basic" <?php if ($see == 'basic'){echo 'checked="checked"';} ?>>Basic things
              <input type="radio" name="see" value="ginfo" <?php if ($see == 'ginfo'){echo 'checked="checked"';} ?>>General info
            </p>
            <button type="submit" class="btn btn-default">Ver</button> 
        </form>

    <?php 
}else if ($site == 'funcs'){
  ?>
  <h4>Select lang to show</h4>
  <form method='get'>
    <p>Lang to show: 
        <select name="lang" style="min-width:100px;">
          <option value="php" <?php if ($lang == 'php'){echo 'selected="selected"';} ?>>PHP</option>
          <option value="kotlin" <?php if ($lang == 'kotlin'){echo 'selected="selected"';} ?>>Kotlin</option>
        </select>
    <button type="submit" class="btn btn-default">Change</button> </p>
  </form>
  <form method="GET">
    <p>Search:  <input onkeyup="searchAjax();" type="text" name="search" size="15"/></p>
  </form>
    <!-- Script for searching -->
    <script>
    function searchAjax(){
      var searchText = document.getElementsByName("search")[0].value.trim();
      var xhr = new XMLHttpRequest();

        if (searchText.length > 2){
          xhr.open('GET', 'searchhelper.php?what=func&lang=<?php print $lang;?>&str='+searchText);
          if (document.getElementById("nosearch")){
            document.getElementById("nosearch").style.display = 'none'; /* Not display standard */
          }
        }else{
          document.getElementById("searchresult").innerHTML = "<p>Debes escribir más de dos caracteres</p>";
          if (document.getElementById("nosearch")){
            document.getElementById("nosearch").style.display = 'block'; /* Display all functions */
          }
        }
      
      xhr.onload = function() {
        if (xhr.status === 200) {
          document.getElementById("searchresult").innerHTML = xhr.responseText;
        }
      };
      if (searchText.length > 2){
        xhr.send();
      }
    }
    </script>
  <?php
}
?>
  </div>
</div>