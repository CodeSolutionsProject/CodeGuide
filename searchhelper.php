<?php
$searchstr = $_GET['str'];
$send = "";
$lang = $_GET['lang'];
if ($_GET['what'] == "func"){ //Si es el buscador rápido de newmidi.php (Tanto para nuevas como las del cajón)
    $send .= "<p>Mostrando funciones que contengan $searchstr...</p>";
    $funcs = scandir("langs/$lang/functions/");
    foreach ($funcs as $file){
        if (strpos($file, $searchstr) !== false){
            $file = explode('.',$file)[0];
            $send .= "<p><a href='?lang=$lang&func=$file'>$file</a></p>";
        }
    }
}
echo $send;