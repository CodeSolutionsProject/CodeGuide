# Variables

## Declare and set variable
- First line, declare. Second line, set. Third, declare and set.
. variable

## Declare and set constant
- Same
. constant

## Set array and show position 1 of that array
. array