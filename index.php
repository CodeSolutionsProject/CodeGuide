<?php
$site = 'index'; /* For menus */
if (isset($_GET['page'])){
    $site = $_GET['page'];
}
/* First convert post to get if needed */
$lang = isset($_GET['lang']) ? $_GET['lang'] : 'php';
$see = isset($_GET['see']) ? $_GET['see'] : 'basic';


/* Show header */
require_once 'assets/html/header.php';


switch ($see){
    case 'ginfo':
        echo "<h3>General info for $lang</h3>";
        echo '<div class="pairlang">';     
            require_once "lib/parsedown.php";
            echo '<div class="showone">';
            if (file_exists("langs/$lang/general.md")){

                $filemd = file("langs/$lang/general.md", FILE_IGNORE_NEW_LINES);
                array_shift($filemd);
                $filemd = implode(PHP_EOL,$filemd);
                echo Parsedown::instance()
                    ->setMarkupEscaped(true) # escapes markup (HTML)
                    ->text($filemd);
            }else{
                echo '<h4>No hay información disponible</h4>';
            }
            echo '</div>';
        echo '</div>';

    break;

    case 'basic':
    echo "<h3>Basic things of $lang</h3>";
    if (!file_exists('guide.md') || !file_exists("langs/$lang/basic.json")){
        die("<p>Some files not found</p>");
    }
    $file = fopen('guide.md', "r");
    $start = 0;
    $jsonbasic = json_decode(file_get_contents("langs/$lang/basic.json"),true);
        while (($line = fgets($file)) !== false) {
            //See if line have '# '
            if (substr( $line, 0, 2 ) == '# '){
                echo '<h3>';
                echo substr($line,2).'</h3>';
            }
            else if (substr( $line, 0, 3 ) == '## '){
                $start = 1;
                echo '<h4 class="alignleft">';
                echo substr($line,3).'</h4>';
            }
            else if (substr($line,0,2) == '- '){
                echo '<p class="mitalic">'. substr($line,2).'</p>';
            }
            else if ((substr( $line, 0, 2 ) == '. ') && $start == 1){
                echo '<div class="pairlang">';
                $start = 0;
                    echo '<div class="showone">';

                    // See if exists and load it
                    if (isset($jsonbasic[trim(substr($line,2))])){
                        echo '<pre><code>';
                        if (is_array($jsonbasic[trim(substr($line,2))])){
                            foreach($jsonbasic[trim(substr($line,2))] as $c)
                            {
                                echo "$c".PHP_EOL;
                            }
                        }else{
                            print $jsonbasic[trim(substr($line,2))];
                        }
                        echo '</code></pre>';
                    }else {
                        echo '<pre>This can\'t be do in this lang, or it isn\'t added yet to this guide.</pre>';
                    }
                    echo '</div>';
                echo '</div>';
            }
        }
        fclose($file); /* Close guide.md */

    break;

    case 'sql':
    if (!file_exists("langs/$lang/database.json")){
        break;
    }
    $jsonbasic = json_decode(file_get_contents("langs/$lang/basic.json"),true);

    break;

    default: 
        echo '<p>Default</p>';
    break;
}

require_once 'assets/html/footer.php';